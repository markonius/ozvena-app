package com.markonius.ozvena

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.webkit.CookieManager
import android.webkit.WebView


class MainActivity : Activity() {
    companion object {
        const val PREFS_FILE = "HashStore"
        const val PREF_HASH = "Hash"
    }

    private var webView: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_main)

        webView = findViewById(R.id.webView)
        webView?.webViewClient = OzvenaWebViewClient(this)
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.domStorageEnabled = true
        webView?.addJavascriptInterface(JSInterface(this), "Android")
        if (savedInstanceState != null)
            webView?.restoreState(savedInstanceState)
        else
            webView?.loadUrl("https://markonius.gitlab.io/ozvena/ozvena.html")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        webView?.saveState(outState)
        super.onSaveInstanceState(outState)
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    override fun onPause() {
        super.onPause()
        CookieManager.getInstance().flush()
    }
}
