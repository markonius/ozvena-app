package com.markonius.ozvena

import android.content.Context
import android.webkit.JavascriptInterface
import android.widget.Toast

class JSInterface(
        private val context: Context
    ) {

    @JavascriptInterface
    fun saveKey(bigHash: String?){
        val prefs = context.getSharedPreferences(MainActivity.PREFS_FILE, 0)
        val editor = prefs.edit()
        editor.putString(MainActivity.PREF_HASH, bigHash)
        editor.apply()
    }
}