package com.markonius.ozvena

import android.content.Intent
import android.util.Base64
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast

class OzvenaWebViewClient(
        private val mainActivity: MainActivity
    ) : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        return if (request != null
            && request.url.host == "markonius.gitlab.io"
            && request.url.pathSegments.contains("ozvena")
            && !request.url.pathSegments.contains("LICENSE.txt")
        ) {
            false
        } else {
            val intent = Intent(Intent.ACTION_VIEW, request?.url)
            mainActivity.startActivity(intent)
            true
        }
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        tryLoadHash(view)
    }

    private fun tryLoadHash(webView: WebView?) {
        val prefs = mainActivity.getSharedPreferences(MainActivity.PREFS_FILE, 0)
        val hash = prefs.getString(MainActivity.PREF_HASH, null)
        if(hash != null) {
            val bytes = Base64.decode(hash, Base64.DEFAULT)
            val bytesString = bytes.asIterable().joinToString(",", "[", "]") { (it.toUByte()).toString() }
            Toast.makeText(mainActivity, bytesString, Toast.LENGTH_SHORT)
            webView?.loadUrl("javascript: window.skipMasterPassword($bytesString);")
        }
    }
}